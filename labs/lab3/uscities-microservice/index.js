// LAB 3
// Basic setup for microservice using Express
const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
app.use (express.urlencoded({extended: false}))
const cors = require('cors') // New for microservice
app.use(cors()) // New for microservice
app.listen(port)
console.log("US City Search Microservice is running on port " + port)

// Node.js code to connect to MongoDB Atlas database
const MongoClient = require('mongodb').MongoClient;
const mongourl = "mongodb+srv://cca-mirandar1:SeaKey554@cca-mirandar1.xoiwp.mongodb.net/cca-labs?retryWrites=true&w=majority";
const dbClient = new MongoClient(mongourl, {useNewUrlParser: true, useUnifiedTopology: true});
dbClient.connect(err => {
    if (err) throw err;
    console.log("Connected to the MongoDB cluster");
});

// Base page
app.get("/", (req, res) => {
    res.send("US City Search Microservice by Phu Phung. Usage: URL/uscities-search/:zips(\\\\d{1,5})");
})

// The fields we want returned from matching documents, used in project()
let fields = {_id: false, 
                    zips: true, 
                    city: true,
                    state_id: true,
                    state_name: true, 
                    county_name: true, 
                    timezone: true};

// City Search by Zipcode API
app.get('/uscities-search/:zips(\\d{1,5})', function (req, res) { // only URLs with zips 0 to 99999 will be accepted
    const db = dbClient.db(); // connect to database
    let zipRegEx = new RegExp(req.params.zips); // the accepted zipcode is stored in "zips", which is made into a Regular Expression
        const cursor = 
db.collection("uscities").find({zips:zipRegEx}).project(fields); // the fields we want will be returned from the documents that fulfill the
    cursor.toArray(function(err, results) {                      // zipRegEx conditions (match the zipcode) and stored in cursor
        console.log(results); // output all records for debug only
        res.send(results);
    });
});

// City Search by Name API
app.get('/uscities-search/:city', function (req, res) { // parameter "city": accepts whatever is typed
    const db = dbClient.db(); // connect to database
    let cityRegEx = new RegExp(req.params.city,'i'); // 'i' option will cause RegEx to match case-INsensitive inputs ("Dayton" and "dayton")
    const cursor = db.collection("uscities").find({city:cityRegEx}).project(fields); // now want to match "city" field instead of "zips"
    cursor.toArray(function(err, results) {
        console.log(results); //output all records
        res.send(results);
    });
});


