// LAB 4
const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
const server = require('http').createServer(app); // NEW
const io = require('socket.io')(server); // NEW
app.use(express.static('static'))
app.use(express.urlencoded({extended: false}))
server.listen(port); // NEW
console.log("Webchat server is running on port " + port)
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/static/chatclient.html');
})

// Event handling

// "connection" event: New client connected => send alert showing they connected and show number of connected clients
// and handle other events
io.on('connection', (socketclient) => { // newest version equivalent is "socket.on("connect",..."
    console.log('A new client is connected!'); 
    /* code to handle */
    var onlineClients = Object.keys(io.sockets.sockets).length;
    var welcomemessage = `${socketclient.id} is connected! Number of connected clients: ${onlineClients}`;
    console.log(welcomemessage);
    io.emit("online", welcomemessage); // emits data to socketio.on("online") in front end which updates html with welcomemessage

    // "message" event: Client sent message => broadcast message to all connected clients
    socketclient.on("message", (data) => {
        console.log('DEBUG: Data from a client: ' + data);
        io.emit("message", `${socketclient.id} says: ${data}`); // message broadcasted to all clients on site showing who sent it
    });

    // "typing" event: A client is typing => show [clientId] is typing
    socketclient.on("typing", () => {
        // FOR DEBUGGING ONLY, COMMENT OUT BEFORE DEPLOYING
        console.log('Someone is typing...');
        // send to all connected clients
        io.emit("typing", `${socketclient.id} is typing...`);
    });

    // "disconnect" event: A client disconnected => send alert saying they disconnected and show number of connected clients
    socketclient.on("disconnect", () => {
        var onlineClients = Object.keys(io.sockets.sockets).length;
        var byemessage = `${socketclient.id} is disconnected! Number of connected clients: ${onlineClients}`;
        console.log(byemessage);
        io.emit("online", byemessage);
    });
 });