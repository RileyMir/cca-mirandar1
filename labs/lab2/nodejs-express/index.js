const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
app.use(express.static('static'))
app.use (express.urlencoded({extended: false}))
const cors = require('cors') // New for microservice
app.use(cors()) // New for microservice
app.listen(port, () =>
    console.log(`HTTP Server with Express.js is listening on port:${port}`))

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/static/cca-form.html');
})
app.get('/echo.php', function (req, res) {
    var data = req.query.data
    res.send(data)
})
app.post('/echo.php', function (req, res) { // include slash
    var data = req.body['data'] // accesses 'data' property of request object sent: {data: input}
    res.send(data)              // equivalent to req.body.data
})