var http = require('http');
var server = http.createServer(httpHandler);
var port = process.env.PORT || 8080;
server.listen(port);
console.log("httpserver is running at port: " + port);
const fs = require('fs'), url = require('url');
function httpHandler(request, response) {
    console.log('Get an HTTP Request: ' + request.url);
    response.writeHead(200, {'Content-Type': 'html/text'});
    response.write("You have requested " + request.url);
    return response.end();
}