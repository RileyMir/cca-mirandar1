var http = require('http');
var server = http.createServer(httpHandler);
var port = process.env.PORT || 8080;
server.listen(port);
console.log("httpserver is running at port: " + port);
const fs = require('fs'), url = require('url');
function httpHandler(request, response) {
    console.log('Get an HTTP Request: ' + request.url);
    var fullpath = url.parse(request.url, true);
    var localfile = '..' + fullpath.pathname // we serve files in the parent folder (..)
    console.log("Debug: Server's local file: " + localfile);
    fs.readFile(localfile, (error, filecontent) => {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(filecontent);
        return response.end();
    })
}