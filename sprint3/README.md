# CCA Project Repository 2021

## CCA - Cloud Computing and Applications

## Instructor: Dr. Phu Phung

### Department of Computer Science
### University of Dayton

# Information

This is a private repository for CCA - Cloud Computing and Applications class.

# Project Members
## Riley Miranda
Email: cca-mirandar1@hotmail.com

## TJ Nicholson
Email: nicholsont1@udayton.edu