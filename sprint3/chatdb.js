const MongoClient = require('mongodb').MongoClient;
const mongourl = "mongodb+srv://cca-project-db-account:cloud1010@cca-mirandar1.xoiwp.mongodb.net/cca-project?retryWrites=true&w=majority";
const dbClient = new MongoClient(mongourl, {useNewUrlParser: true, useUnifiedTopology: true});
dbClient.connect(err => {
    if (err) throw err;
    console.log("Connected to the MongoDB cluster");
});
/**
 * API 1: Store a Private Chat Message
 * Input: sender, receiver, message, auto-generated timestamp
 * Store document in chat history collection. No output
 */
module.exports.storePrivateMessage = (privatechat) => { //privatechat = { sender, receiver, message, timestamp }
    // Validation: check privatechat has no empty fields
    const db = dbClient.db();
    for(var key in privatechat) {
        if (privatechat[key] === "") { return; }
    }
    db.collection("users").find({username: { $or: [ {sender: privatechat.sender, receiver: privatechat.receiver}, 
    {sender: privatechat.receiver, receiver: privatechat.sender} ] } }, (err, areUsers) => {
        if (areUsers) { //users are in db
            db.collection("chat_history").insertOne(privatechat, (err, result) => {
                if (err) { console.log("ERROR: Private chat not stored"); }
                else { console.log("DEBUG: Private chat stored"); }
            });
        } else { console.log("Error: one or both users not found in db"); }
    });
}



 /**
  * API 2: Retrieve Private Chat Messages
  * Input: user1 (sender), user2 (receiver)
  * Output: latest chat history (array) in the collection between the sender and receiver (user1 sent to user2 OR user2 sent to user1)
  */
 module.exports.loadPrivateMessage = (sender, receiver, callback) => {
    const db = dbClient.db();
    //TODO: Need to validate whether sender/receiver are valid users in the "users" database? (see comment on ln 53)
    //Query the database collection, and return the results via the callback function
    db.collection("chat_history")
                .find({ $or: [{sender:sender, receiver:receiver}, {sender:receiver, receiver:sender}]})
                .sort({timestamp:1})
                .limit(100).toArray((err,history) => {
                    if(err || !history) {
                        console.log("Error returning history or no history")
                        return callback(false,null);
                    } else {
                        console.log("Chatdb.js returned history");
                        return callback(true,history);
                    }
                });
    /** 
     * Concept for how to validate if sender and receiver are valid users, 
     * but I think this will be valid if one is a valid user even if the other isn't
     * b/c of how the $or is currently used. Use $and instead?
     * -It works right now because there is no situation in which a user could click on/query an invalid user
     */
    // db.collection("users").find({$or: [{username:sender}, {username:receiver}]},(err,user) => {
    //     if(err || !user) {
    //         //error or sender/receiver not found as valid users
    //         console.log(`ERROR: Sender:Receiver (${sender}:${receiver}) not found for Private Chat History`);
    //         return callback(false, null);
    //     } else {
    //         console.log(`Users (Sender:${sender} Receiver:${receiver}) found, returning chat history`)

    //     }
    // });            
}

