const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
const server = require('http').createServer(app);
const https = require('https');
const superagent = require('superagent');
const io = require('socket.io')(server);
server.listen(port);
app.use(express.static('static'))
app.use(express.urlencoded({extended: false}))
console.log("WebChat server is running on port " + port);
// NEW SPRINT 3
var chatdb = require('./chatdb');
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/static/chatclient.html');
})
/**
 * For testing that only authenticated users can send messages
 */
// app.get('/chatbot', (req, res) => {
//     res.sendFile(__dirname + '/static/chatbot.html');
// })

io.on('connection', (socketclient) => {
    //The following 4 lines are only for serverside debugging, proper UI welcome message now displays on login
    console.log('A new client is connected!');
    var onlineClients = Object.keys(io.sockets.sockets).length;
    var welcomemessage = `${socketclient.id} is connected! Number of ALL connected clients: ${onlineClients}`;
    console.log(welcomemessage);
    /**
     * PRIVATECHAT
     */
    socketclient.on("privatechat", (receiver, message) => {
        if (socketclient.authenticated) { //prevent chatbot.html (inauthenticated user) from sending messages from an authenticated account
            let sender = socketclient.username;
            var timestamp = new Date(); //Generate timestamp
            let privatechat = {sender, receiver, message, timestamp}; //Format for chatdb
            if(receiver == "Chatbot") {
                chatdb.storePrivateMessage(privatechat);
                chatbotReceivedMessage(sender, message);
            } else {
                var sockets = io.sockets.sockets;
                for (var id in sockets) {
                    let socketclient = sockets[id];
                    if (socketclient && socketclient.authenticated && socketclient.username==receiver) {
                        socketclient.emit("privatechat", sender, message);
                        chatdb.storePrivateMessage(privatechat);
                    }
                }
            }
            
        }
    });
    /**
     * CHAT HISTORY
     */
    socketclient.on("chathistory", (currentUser, username) => {
        let sender = currentUser;
        let receiver = username;
        chatdb.loadPrivateMessage(sender, receiver, (gotHistory, chathistory) => {
            if (!gotHistory) {console.log("Error: Chat history not returned.");}
            else {
              socketclient.emit("chathistory", chathistory, username);}
              console.log("Chat History Calledback");
          }); 
    });
    /**
     * MESSAGE
     */
    socketclient.on("message", (data) => {
        /**
         * Broadcast messages only to authenticated users
         */
        if(socketclient.authenticated && socketclient.username) {
            console.log(`Data from ${socketclient.username}:  ${data}`);
            BroadcastAuthenticatedClients("message", `${socketclient.username} says: ${data}`);
        }
    });
    /**
     * TYPING
     */
    socketclient.on("typing", () => {
        /**
         * Broadcast typing message only to authenticated users
         * that aren't the person who is typing
         */
        if(socketclient.authenticated) {
            console.log(`${socketclient.username} is typing...`);
            BroadcastAuthenticatedNonSelfClients(socketclient.username,"typing", `${socketclient.username} is typing ...`);
        }
    });    
    socketclient.on('disconnect', () => {
        //Following two lines are only for serverside debugging of checking number of ALL connected clients
        var onlineClients = Object.keys(io.sockets.sockets).length;
        console.log(`${socketclient.id} is disconnected! Number of ALL clients online: ${onlineClients}`);
        /**
         * Since the 'disconnect' event is triggered on any client disconnect,
         * need to check that the client was an authenticated client 
         * before broadcasting, otherwise the count will be wrong/client will not
         * have a username property which would cause an error
         */
        if(socketclient.authenticated && socketclient.username) {
            var byemessage = `${socketclient.username} is disconnected! Number of Authenticated Clients Online: ${NumberOfOnlineAuthenticatedClients()}`;
            console.log(byemessage);
            BroadcastAuthenticatedClients("online", byemessage)
            // update userlist
            getUserList();
        }
        
    });
    /**
     * Signup Event/Microservice Call
     */
    socketclient.on("signup", (account) => {
        console.log('Signup Data from client: ' + JSON.stringify(account));
        var username = account.username;
        var password = account.password;
        var fullName = account.fullName;
        var email = account.email;
        signup(username,password,fullName,email,(userCreated,message)=>{
            if(userCreated) {
                socketclient.emit("user-created", message);
                console.log(`Debug: User: ${username}/${password} has been successfully created`);
            } else {
                socketclient.emit("user-not-created", message);
                console.log(`Debug: User: ${username}/${password} creation FAILED`);
            }
        })
    });
    /**
     * Login Event/Microservice Call
     */
    socketclient.on("login", (username, password) => {
        console.log(`Login Data from client: ${username}/${password}`);
        login(username,password,(authenticated,message, profile)=>{
            if(authenticated) {
                socketclient.authenticated = true;
                socketclient.username = profile.username;
                // for setting current user and displaying userlist
                socketclient.profile = {username: profile.username, fullname: profile.fullName, avatar: 'https://i.pravatar.cc/150?u=' + profile.username};
                socketclient.emit("authenticated",message, socketclient.username);
                console.log("DEBUG: socketclient emitted setCurrentUser( " + JSON.stringify(socketclient.profile) + " )");
                socketclient.emit("setCurrentUser", socketclient.profile); // These need to be called after the "authenticated" emission so the chatbox library
                getUserList();                                             // is appended before the functions from the api are called
                console.log(`Debug: ${username} is authenticated`)
                BroadcastAuthenticatedClients("online", `Welcome, ${username}, to the chat room! Number of Authenticated Clients Online: ${NumberOfOnlineAuthenticatedClients()}`);
            } else {
                socketclient.emit("login-failed",message);
                console.log(`Debug: Login Failed for: ${username}/${password}`)
            }
        })
        
    });
});
function signup(username,password,fullName,email,callback) {
    /**
     * POST REQUEST TO MICROSERVICE USING NODEJS BUILT-IN HTTPS 
     * Referencing: https://nodejs.org/api/https.html#https_https_request_options_callback
     */
    var account = `username=${username}&password=${password}&fullName=${fullName}&email=${email}`;
    var options = {
        hostname: 'cca-chatdatabase-microservices.herokuapp.com',
        port: 443,
        path: '/signup',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': account.length
        }
    };
    const request = https.request(options, (response) => {
        console.log(`POST status code: ${response.statusCode}`);
        console.log(`POST headers: ${JSON.stringify(response.headers)}`);
        response.setEncoding('utf8');
        let data = '';        
        response.on('data', (chunk) => {
            data += chunk;
        });
        response.on('end', () => {
            console.log('No more data in response.');
            console.log(data);
            const result = JSON.parse(data);
            console.log("REGISTRATION: Response from server:" + JSON.stringify(result));
            if(!result || result.status == "fail" || result.status == "invalid") {
                /**
                 * if signup fails or a data field has an invalid entry, 
                 * userCreated should be false and callback the 
                 * failed signup message from microservice
                 */
                return callback(false,result.message);
            }
            if(result && result.status == "success") {
                /**
                 * if signup succeeds, userCreated should be true and callback the
                 * success message from microservice
                 */
                return callback(true,result.message);
            }
        });
    });
    //Log Error
    request.on('error', (err) => {
    console.error(`problem with request: ${err.message}`);
    });
    //Write data to request body
    request.write(account);
    request.end();
}
function login(username,password,callback) {
    https.get('https://cca-chatdatabase-microservices.herokuapp.com/login/'+username+'/'+password, (response) => {
        let data = '';
        response.on('data', (chunk) => {
            data += chunk;
        });
        response.on('end', () => {
            const result = JSON.parse(data);
            console.log("DEBUG: result: " + result);
            if(!result || result.status == "notfound" || result.status == "invalid") {
                /**
                 * if the user entered isn't found: do not authenticate, callback the respective 
                 * not-found message from microservice, and there is no user to callback so set to null
                 */
                callback(false,result.message,null)
            } else {
                if(result && result.status == "found") {
                /**
                 * if the user entered is found: authenticate, callback the respective found 
                 * message from microservice, and the username that has been found (slightly redundant)
                 */
                    return callback(true,result.message,result.user) // callback now passes user object
                }
            }
        });
    });
    /**
     * FOR TESTING
     */
    // if(username === "test") {
    //     return callback(true,null,username);
    // } else {
    //     callback(false,message,null);
    // }
}
/**
 * Broadcast an event with a message only to authenticated clients
 * @param {An event to be emitted} event 
 * @param {A message to accompany the event} message 
 */
function BroadcastAuthenticatedClients(event,message) {
    var sockets = io.sockets.sockets;
    for (var id in sockets) {
        const socketclient = sockets[id];
        if(socketclient && socketclient.authenticated) {
            socketclient.emit(event,message);
        }
    }
}
/**
 * Broadcast an event with a message only to authenticated clients that
 * aren't the sender
 * Primary use case: "___ is typing" message
 * @param {socketclient.username of the client who emitted the event} senderClientUsername 
 * @param {An event to be emitted} event 
 * @param {A message to accompany the event} message 
 */
function BroadcastAuthenticatedNonSelfClients(senderClientUsername,event,message) {
    var sockets = io.sockets.sockets;
    for (var id in sockets) {
        const socketclient = sockets[id];
        if(socketclient && socketclient.authenticated && !(socketclient.username === senderClientUsername)) {
            socketclient.emit(event,message);
        }
    }
}
/**
 * Gather the number of online, authenticated clients
 * (Since var onlineClients = Object.keys(io.sockets.sockets).length; 
 * counts ALL clients, which would give inaccurate counts)
 */
function NumberOfOnlineAuthenticatedClients() {
    var sockets = io.sockets.sockets;
    var counter = 0;
    for (var id in sockets) {
        const socketclient = sockets[id];
        if(socketclient && socketclient.authenticated) {
            counter++;
        }
    }
    return counter;
}
/**
 * Get list of online users (JSON array) and send to front-end (socketclient.emit("displayUserList", userlist))
 * Called on when an authenticated user connects/disconnects
 */
function getUserList() {
    var sockets = io.sockets.sockets;
    // Create JSON array of connected authenticated users (and chatbot)
    let onlineUsers = new Map();
    // Set Chatbot User as an online user (before user loop so it is always at the top of the online user list)
    // Normal Profile syntax for reference: {username: profile.username, fullname: profile.fullName, avatar: 'https://i.pravatar.cc/150?u=' + profile.username}
    var chatbotProfile = {username: "Chatbot", fullname: "Chatbot", avatar: '/chatbot.jpg'};
    onlineUsers.set(chatbotProfile.username, chatbotProfile, chatbotProfile.fullname);
    for (var id in sockets) {
        const socketclient = sockets[id];
        if (socketclient && socketclient.authenticated && socketclient.profile) {
            onlineUsers.set(socketclient.username, socketclient.profile, socketclient.fullName);
        }
    } console.log("DEBUG: onlineUsers: " + JSON.stringify(onlineUsers));// Update each connected authenticated user's userlist
    for (var id in sockets) {
        const socketclient = sockets[id];
        if (socketclient && socketclient.authenticated && socketclient.profile) {
            let onlineUsersJSON = [];
            onlineUsers.forEach((value, key) => {
                onlineUsersJSON.push(value);
            });
            socketclient.emit("displayUserList", onlineUsersJSON);
        }
    }
}
/**
 * ---------------------------------------------------------------------------------------------------------------------------
 * CHATBOT IMPLEMENTATION
 * -code before this has:
 * ln 293-296 > set chatbot as a valid online user
 * ln 38-41 > allow chatbot to receive private messages and store those messages in chat_history
 * ---------------------------------------------------------------------------------------------------------------------------
 */


/**
 * CHATBOT SCRIPT
 */
/**
 * Gathering/Analysing/Constructing appropriate response based off of a user's message to the chatbot
 * @param {The user who sent the initial message to chatbot} sender 
 * @param {The message received by the chatbot} message 
 */
function chatbotReceivedMessage(sender, message) {
    /**
     * This is where different messages can get handled. Can call other functions or can just be a giant set of if/elses
     * -could use switch statement here
     * should eventually respond to user using: 
     *          socketclient.emit("privatechat", sender, message);
     *      and store chatbot's response in chathistory using:
     *          let privatechat = {sender, receiver, message, timestamp}; //Format for chatdb
     *          chatdb.storePrivateMessage(privatechat);
     *      then return;
     */
    /**For Reference:
     * Microservice 1 (Days and Weeks Until) takes one input (a future year) and returns a string with days and weeks until that year.
     * Microservice 2 (AdjectiveAgeGen) takes two inputs (a letter and a date) and returns a string with a positive adjective starting with the given letter and the days alive (days since that date).
     */

     /**Following lines extract:
      * 1) A 'trigger' message to call one of the microservices
      *     Microservice 1: "How many days until <year>"
      *     Microservice 2: any message with a single letter enclosed by quotes and a date in the format of MM/DD/YYYY
      * 2) The input(s) for each microservice
      * 
      * To add another phrase for the chatbot:
      * 1) Add a regular expression variable with the literal phrase and the regex for matching a single letter, year, or date, which are already shown
      * 2) Add another case to the switch statement like so:
      *     case <yourRegExVariableName>.test(message):
      *         --get year/letter and date here--
      *         --call the microservice calling function here with the right parameters--
      *         break;
      */
    //DEBUG
    //TODO: Make message collection ignore newline characters
    console.log(`DEBUG>> Chatbot received the message: ${message}`);

    /**
     * NOTE: chatbot's sending of messages back to the user are handled in 
     * the function chatbotSendMessage(chatbotResponse) which is used frequently
     * in the code below. See that function for further details about chatbot reponses
     */
    
    // Add more RegEx here
    // Microservice 1 trigger phrases
    const dateRegex = /\d{2}\/\d{2}\/\d{4}/;
    const errorMes = "Sorry, I don't understand or I can't compute that";
    const micro1_0 = /How many days until \d{4}/;
    const micro1_1 = /How many weeks until \d{4}/;
    const micro1_2 = /How many days and weeks until \d{4}/;
    const micro1_3 = /How many weeks and days until \d{4}/;

    // Microservice 2 Regular Expression triggers
    const micro2RE1 = RegExp(/"([^"]*)"/, 'i') //"letter" i.e. "a" (case insensitive)
    const micro2RE2 = RegExp(dateRegex); //Date of the format MM/DD/YYYY
    //This variable is only true if there is only 1 letter between the quotation marks and there is a date of the correct format
    let micro2 = (micro2RE1.test(message) && micro2RE2.test(message) && micro2RE1.exec(message)[0].length == 3);
    //Debugging
    if(micro2RE1.test(message) && micro2RE1.exec(message)[0].length == 3) { //checking for length of 3 because a correct string stored looks like: "a"
        console.log("DEBUG> Chatbot detected letter: " + micro2RE1.exec(message)[0].substring(1,2));
    }
    if(micro2RE2.test(message)) {
        console.log("DEBUG> Chatbot detected date: " + micro2RE2.exec(message)[0]);
    }
    if(micro2) {
        console.log("DEBUG>> Both parameters for Microservice 2 Detected, Calling AdjectiveAgeGen at https://cca-team6-microservice2.herokuapp.com/AdjectiveAgeGen?letter=letter&bday=date");
    }

    //Constructing the appropriate response to the given message
    switch (true) {
        //Handle !help message
        case message === "!help":
            chatbotSendMessage("Hello! To call Microservice 1 (DaysWeeksUntil) enter \"How many days and weeks until ####\"." 
            + "To call Microservice 2 (AdjectiveAgeGen) enter any message with a letter in quotes i.e. \"a\" and a date of the format MM/DD/YYYY");
            break;
        case micro1_0.test(message):
            console.log("DEBUG: Microservice 1 (Days and Weeks Until) called at https://cca-mirandar1-nicholsont1.azurewebsites.net/DaysWeeksUntil?year=year");
            callMicro1(sender, message.match(/\d{4}/));
            break;
        case micro2:
            callMicro2(sender, micro2RE1.exec(message)[0].substring(1,2), micro2RE2.exec(message)[0]);
            break;
        case micro1_1.test(message):
            console.log("DEBUG: Microservice 1 (Days and Weeks Until) called at https://cca-mirandar1-nicholsont1.azurewebsites.net/DaysWeeksUntil?year=year");
            callMicro1(sender, message.match(/\d{4}/));
            break;
        case micro1_2.test(message):
            console.log("DEBUG: Microservice 1 (Days and Weeks Until) called at https://cca-mirandar1-nicholsont1.azurewebsites.net/DaysWeeksUntil?year=year");
            callMicro1(sender, message.match(/\d{4}/));
            break;
        case micro1_3.test(message):
            console.log("DEBUG: Microservice 1 (Days and Weeks Until) called at https://cca-mirandar1-nicholsont1.azurewebsites.net/DaysWeeksUntil?year=year");
            callMicro1(sender, message.match(/\d{4}/));
            break;
        default: // sends error message
            chatbotSendMessage(errorMes);
    }
    return;

    /**
     * Chatbot responds to user with whatever is entered as a parameter (ideally a simple string)
     * Used throughout following code for chatbot responses
     * @param {What the chatbot will send back to the user} chatbotResponse 
     */
    function chatbotSendMessage(chatbotResponse) {
        var timestamp = new Date(); //Generate timestamp
        var sockets = io.sockets.sockets;
                for (var id in sockets) {
                    let socketclient = sockets[id];
                    if (socketclient && socketclient.authenticated && socketclient.username==sender) {
                        socketclient.emit("privatechat", "Chatbot", chatbotResponse);
                    }
                }
                let privatechat = {"sender": "Chatbot", "receiver": sender, "message": chatbotResponse, timestamp}; //Format for chatdb
                chatdb.storePrivateMessage(privatechat);
    }

    /**
     * CHATBOT Microservice 1 & 2 Calls
     */
    function callMicro1(sender, year) {
        https.get('https://cca-mirandar1-nicholsont1.azurewebsites.net/DaysWeeksUntil?year=' + year,
        (response) => {
            let data = '';
            response.on('data', (chunk) => {
                data += chunk;
            });
            response.on('end', () => {
                const result = data;
                if (!result) {
                    console.log("DEBUG> Chatbot responded with msg: " + errorMes);
                    chatbotSendMessage(errorMes);
                } else {
                    console.log("DEBUG> Chatbot responded with msg: " + result);
                    chatbotSendMessage(result);
                }
            })
        })
    }

    function callMicro2(sender, letter, date) {
        https.get('https://cca-team6-microservice2.herokuapp.com/AdjectiveAgeGen?letter='+letter+'&bday='+date,
        (response) => {
            let data = '';
            response.on('data', (chunk) => {
                data += chunk;
            });
            response.on('end', () => {
                var timestamp = new Date(); //Generate timestamp
                const result = data;
                if (!result) {
                    console.log("DEBUG> Chatbot responded with msg: " + errorMes);
                    chatbotSendMessage(errorMes);
                } else {
                    console.log("DEBUG> Chatbot responded with msg: " + result);
                    chatbotSendMessage(result);
                }
            })
        })
    }

} //end of chatbotReceivedMessage() (this wraps chatbotSendMessage(), callMicro1(), and callMicro2() so that those functions can access the sender variable)

